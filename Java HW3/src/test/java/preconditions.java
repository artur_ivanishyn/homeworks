import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

public class preconditions {
    WebDriver driver;

    String URL = "https://user-data.hillel.it/html/registration.html";

    @BeforeClass
    public static void mainPreconditions() {
        //System.setProperty("webdriver.gecko.driver","C:\\Users\\User\\Desktop\\exampleAutomation\\geckodriver.exe");
        System.setProperty("webdriver.chrome.driver2","C:\\Users\\User\\Desktop\\exampleAutomation\\chromedriver.exe");
    }

    @Before
    public void precondition(){
        //driver = new FirefoxDriver();
        driver = new ChromeDriver();
    }

    @After
    public void after() throws InterruptedException {
        driver.quit();
    }

    @AfterClass
    public static void afterClass(){
    }

    public void getRegistration(){
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".registration"))).click();
    }

    public void getLoginAndInfo(String email, String pass) throws InterruptedException {
        driver.findElement(By.id("email")).sendKeys(email);
        driver.findElement(By.id("password")).sendKeys(pass);
        driver.findElement(By.cssSelector(".login_button")).click();
        Thread.sleep(300);
        driver.findElement(By.id("info")).click();
        Thread.sleep(500);
    }

    public void setFirstName(String firstName){
        driver.findElement(By.cssSelector("#first_name")).sendKeys(firstName);
    }

    public void setLastName(String lastName){
        driver.findElement(By.cssSelector("#last_name")).sendKeys(lastName);
    }

    public void setData(String phone, String mobile, String email, String pass){
        driver.findElement(By.id("field_work_phone")).sendKeys(phone);
        driver.findElement(By.id("field_phone")).sendKeys(mobile);
        driver.findElement(By.id("field_email")).sendKeys(email);
        driver.findElement(By.id("field_password")).sendKeys(pass);
    }

    public static String getNumber(){
        String charS = "0123456789";
        Random rOnd = new Random();
        String numb="";
        for(int i=0;i<7;i++) {
            char c = charS.charAt(rOnd.nextInt(charS.length()));
            numb=numb+c;
        }

        String[] numb2 = new String[]{"38067", "38063", "38066", "38050"};
        Random rand = new Random();
        int position = rand.nextInt(numb2.length);
        String num = numb2[position];

        String number = num + numb;
    return number;
    }

    public static String getName(){
        String[] arrayNames = new String[]{"Artur", "Alan", "Glen", "Miles", "Sam", "Frodo", "Bilbo", "Dambledor"};
        Random rand1 = new Random();
        int position1 = rand1.nextInt(arrayNames.length);
        String firstName = arrayNames[position1];
        return firstName;
    }

    public static String getLName(){
        String[] arrayLastNames = new String[]{"Konan_Doyle", "Rickman", "Miller", "Kennedy", "Rokwell", "Baggins", "Breiher", "Turing"};
        Random rand2 = new Random();
        int position2 = rand2.nextInt(arrayLastNames.length);
        String lastName = arrayLastNames[position2];
        return lastName;
    }

    public static String getEmail(){
        String chars = "abcdefghijklmnopqrstuvwxyz";
        Random rand3 = new Random();
        String symbols="";
        for(int i=0;i<10;i++) {
            char c = chars.charAt(rand3.nextInt(chars.length()));
            symbols=symbols+c;
        }

        String[] arrayLastNames = new String[]{"mail.ru", "yahoo.com", "gmail.com", "rambler.ru", "meta.ua"};
        Random rand4 = new Random();
        int position4 = rand4.nextInt(arrayLastNames.length);
        String mail = arrayLastNames[position4];

        return symbols+"@"+mail;
    }

}
