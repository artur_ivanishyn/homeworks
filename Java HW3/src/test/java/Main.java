import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import java.util.Random;
import java.util.List;


public class Main extends preconditions{

    @Test
    public void theTEST() throws InterruptedException {

        String firstName = getName();
        String lastName = getLName();
        String number = getNumber();

        String email= getEmail();
        String pass="Qwerty12";

        open (URL);
        getRegistration();
        driver.manage().window().maximize();

        setFirstName(firstName);
        setLastName(lastName);
        setData("7654321",number,email,pass);

        driver.findElement(By.id("male")).click();

        WebElement dropDown = driver.findElement(By.id("position"));
        Select dropD = new Select(dropDown);
        dropD.selectByIndex(2);
        driver.findElement(By.id("position")).click();

        driver.findElement(By.id("button_account")).click();
        Thread.sleep(500);
        driver.switchTo().alert().accept();
        Thread.sleep(500);
        driver.navigate().refresh();

        getLoginAndInfo(email,pass);

        String tn  = driver.findElement(By.id("mobile_phone_my_info")).getAttribute("value");
        Assert.assertEquals("Not equals",number,tn);

        String tEmail  = driver.findElement(By.id("email_MyInfo")).getAttribute("value");
        Assert.assertEquals("Not equals",email,tEmail);

        String tFName  = driver.findElement(By.id("first_name_myInfo")).getAttribute("value");
        Assert.assertEquals("Not equals",firstName,tFName);

        String tLName  = driver.findElement(By.id("last_name_myInfo")).getAttribute("value");
        Assert.assertEquals("Not equals",lastName,tLName);
        Thread.sleep(300);
    }



    public void open(String url){
        driver.get(URL);
    }

}
