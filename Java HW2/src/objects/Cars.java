package objects;

public class Cars {
    public String car_brand;
    public String model;
    public double yearOfProduction;
    public double length;
    public double height;
    public double weight;
    public double tank;
    public double speed;    //текущая скорость
    public int engine;      //HorsePower - мощность движка


    public Cars(String car_brand, String model, int yearOfProduction,  double length, double height, double weight, double tank, double speed, int engine) {
        this.car_brand = car_brand;
        this.model = model;
        this.yearOfProduction = yearOfProduction;
        this.length = length;
        this.height = height;
        this.weight = weight;
        this.tank = tank;
        this.speed=speed;
        this.engine=engine;
    }

    public double getWeight(){
        return this.weight=weight;
    }
    public void horn(){
        System.out.println("beep beep");
    }

    public double fuel_consumption() { //расход топлива на 100 км
      double k1 = 0;    //коэфициент года. чем старше машина тем больше топлива
        for (int i = 2020; i > yearOfProduction; yearOfProduction++) {
            k1 += 0.1;
        }
      double k2=0; //коэфициент размера, чем больше машина тем больше топлива
        k2 = (weight/(length/height))/140;
      double k3=0;
        for (int i=0; i<engine; i++){
            k3+=0.02;
        }
     double consumption = k1+k2+k3;
       return consumption;
    }

    public double fuel(){ // вывод расхода топлива
        System.out.println("fuel consumption of your car is: " + fuel_consumption() + " liters per 100 kilometers");
        return 0;
    }

    public double get_milege(){
        double v = ((tank / fuel_consumption())*100)*100;
        v=Math.round(v);
        v=v/100;
        System.out.println("you can drive:  " + v + " kilometers");
        return v;
    }

    public double speedConverter(double km){    // перевод км в мили
        km=km/1.60934;
        km=km*100;
        km=Math.round(km);
        km=km/100;
        System.out.println(km + " miles");
        return 0;
    }

    public double speedLimit(){
        if(speed>0 && speed<=99){
            System.out.println("your speed is " + speed + " youre good driver. you in limit");
        }else if (speed>=100 && speed<=119){
            System.out.println("your speed is " + speed + " it's average speed. but still in limit");
        }else if(speed>=120 && speed<=139){
            System.out.println("your speed is " + speed + " you speeding!");
        } else{
            System.out.println("your speed is " + speed + " you exceed the speed! Slow Down!");
        }
        return 0;
    }
}
