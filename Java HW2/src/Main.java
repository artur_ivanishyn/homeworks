import objects.Cars;
import inheritance.Limousine;
import inheritance.Pickup;


public class Main {
    public static void main(String[] args) {


Cars Audi = new Cars("Audi", "A6",2009,2200,1530,1255,80,195,210);
Pickup Ford = new Pickup("Ford","T150",2010,2680,1530,1835,140,110,180, 4); // drive - привод на 4 колеса или на 2
Limousine Lincoln = new Limousine("Lincoln","Limo",1995,4500,1310,3210,90,50,75, 10);

Audi.horn();
Audi.speedLimit();
Audi.fuel();
Audi.get_milege();


Ford.horn();
Ford.speedLimit();
Ford.get_milege();
Ford.fuel();

Lincoln.horn();
Lincoln.speedLimit();
Lincoln.get_milege();
Lincoln.fuel();

Audi.speedConverter(100);


    }
}

