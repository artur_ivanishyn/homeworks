package inheritance;
import objects.Cars;

public class Pickup extends Cars {
     int drive; //привод 4*4 или 2*4

     public Pickup(String car_brand, String model, int yearOfProduction,  double length, double height, double weight, double tank, double speed, int engine, int drive){
         super(car_brand, model, yearOfProduction, length, height, weight, tank, speed, engine);
         this.drive = drive;
     }

    @Override
    public void horn(){
        System.out.println("BEEP --- BEEP ---");
    }

    @Override
    public double fuel(){ // вывод расхода топлива
         double kk;
         if (drive == 2)
         {kk = 2.2;}
         else{
             kk=3.5;
         }
        System.out.println("fuel consumption of your car is: " + (fuel_consumption()+kk) + " liters per 100 kilometers");
        return 0;
    }
}
