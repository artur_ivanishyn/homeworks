package inheritance;
import objects.Cars;

public class Limousine extends Cars {
    int passengersCount;  // вместимость салона

    public Limousine(String car_brand, String model, int yearOfProduction,  double length, double height, double weight, double tank, double speed, int engine, int passengersCount){
        super(car_brand,model,yearOfProduction,length, height, weight, tank, speed, engine);
        this.passengersCount = passengersCount;
    }

    @Override
    public void horn(){
        System.out.println("Ta-dada-da Ta-dadadada");
    }

    @Override
    public double fuel(){ // вывод расхода топлива в зависимости от наполнения салона
        double kk=0;
        for(int i=0; i<passengersCount;i++){ // коефициент увеличения на 1 пассажира
            kk=+0.3;
        }
        System.out.println("fuel consumption of your car is: " + (fuel_consumption()+kk) + " liters per 100 kilometers");
        return 0;
    }


}
